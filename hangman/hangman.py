import os
import random

CATEGORY_PATH = 'category'

#======================================================================================================================
# Game Logic
#======================================================================================================================
class Game:

    WIN = 0
    OVER = 1
    PLAYING = 2

    def __init__(self, word):
        self.word=word
        self.score = 0
        self.lives = 10
        self.wrong_guessed = set()
        self.correct_guessed = set()
        self.mask_index = set()
        self.stat = Game.PLAYING

        self.prepare_game()

    def prepare_game(self):
        for i in range(len(self.word)):
            if self.word[i].isalpha(): self.mask_index.add(i)

    def guess(self, letter):
        """return True if correct guess
           return False if wrong guess
           return None otherwise
        """
        if letter.lower() in self.correct_guessed or letter.lower() in self.wrong_guessed:
            return

        correct = False
        
        for i in range(len(self.word)):
            if letter.isalpha() and letter.lower() == self.word[i].lower():
                correct = True
                self.mask_index.discard(i)
                
        if correct: 
            self.correct_guessed.add(letter.lower())
        else:
            self.wrong_guessed.add(letter.lower())

        self.update_stat()

        return correct

    def update_stat(self):
        if self.lives < 0:
            self.stat = Game.OVER
        elif len(self.mask_index) == 0:
            self.stat = Game.WIN

    def add_score(self, score):
        self.score += score

    def add_lives(self, lives):
        self.lives += lives
        self.update_stat()

#======================================================================================================================
# Load word data
#======================================================================================================================
class WordLoader:

    def __init__(self):
        self.cat_list = dict()
        for filename in os.listdir(CATEGORY_PATH):
            if filename.lower().endswith('.cat'):
                with open(os.path.join(CATEGORY_PATH, filename)) as f:
                    title = f.readline().strip()
                self.cat_list[title] = filename
        
    def get_category_list(self):
        return list(self.cat_list.keys())

    def get_word_from_cat(self, cat):
        word_list = []
        with open(os.path.join(CATEGORY_PATH, self.cat_list[cat])) as f:
            next(f)
            for line in f:
                word_list.append(tuple(line.strip().split(',')))
        return word_list

#======================================================================================================================
# Display game response and result
#======================================================================================================================
def display(game):
    # Show Game Over
    if game.stat == game.OVER:
        print(f'Game Over! Answer is "{game.word}"')
        return

    # Show puzzle text
    for i in range(len(game.word)):
        print('_', end=' ') if i in game.mask_index else print(game.word[i], end=' ')

    # Show game status
    print(f'    score {game.score}, remaining wrong guess {game.lives}, '+
    f'wrong guessed: {", ".join(sorted(game.wrong_guessed))}')

    # Show Game WIN
    if game.stat == game.WIN:
        print("Superb!")
        return

#======================================================================================================================
# Program entry point
#======================================================================================================================
if __name__ == "__main__":

    # Load word data ======================================
    loader = WordLoader()
    cat_list = loader.get_category_list()

    # Select catagory dialog ==============================
    print('Select Category:')
    for cat in cat_list: 
        print(cat)

    cat = ''
    while cat == '':
        cat = cat_list[int(input())-1]

    # Get random word to play =============================
    word_list = loader.get_word_from_cat(cat)
    word, hint = random.choice(word_list)

    print(f'Hint: "{hint}"')
    
    # Game start here! ====================================
    game = Game(word)
    display(game)

    while game.stat == Game.PLAYING:
        guess_correct = game.guess(input())

        if guess_correct == True:
            game.add_score(5)
        elif guess_correct == False:
            game.add_lives(-1)

        display(game)